<?php

namespace App\Http\Controllers;

use App\Product;
use App\Cart;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Array_;
use Session;

class ProductController extends Controller
{
    //
    public function getIndex(){
        $carts = Cart::where('user', Session::get('id'))->get();

        return view('index', ['carts' => $carts->count()]);
    }

    public function getProducts(){
        $products = Product::all();

        return view('shop.products', ['products' => $products]);
    }

    public function getAddCart(Request $request, $id){
        $product = Product::find($id);
        $old_cart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($old_cart);
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
        //dd($request->session()->get('cart', $cart));
        return redirect()->route('products.index');
    }

    public function getShoppingCart(){
        $carts = Cart::where('user', Session::get('id'))->get();

        //if(!Session::has('cart')){
        //    return view('shop.shopping_cart', ['carts' => $carts->count()]);
        //}

        //echo $carts->count(); exit();
        if($carts->count() > 0){
            $old_cart = Session::get('cart');
            $cart = new Cart($old_cart);

            foreach($carts as $c){
                $product = Product::find($c['product']);
                $cart->add($product, $product->id);
                //var_dump($c['product']); exit();
            }
            return view('shop.shopping_cart', ['products' => $cart->items, 'total_price' => $cart->total_price, 'carts' => $carts->count()]);
        }else{
            return view('shop.shopping_cart', ['carts' => $carts->count()]);
        }
    }

    public function getVueProducts(){
        $products = Product::get();
        $carts = Cart::where('user', Session::get('id'))->get();
        //$carts = 3;
        $array = array(
            'products' => $products,
            'carts' => $carts->count(),
        );
        return $array;
    }

    public function getVueProduct($id){
        $products = Product::where('id', $id)->first();
        return $products;
    }

    public function getVueAddCart(Request $request, $id){
        $product = Product::find($id);
        $old_cart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($old_cart);
        //$cart->add($product, $product->id);
        //$request->session()->put('cart', $cart);
        //dd($request->session()->get('cart', $cart));

        $cart->user = Session::get('id');
        $cart->product = $id;
        $cart->save();

        $products = Product::get();
        return $products;
        //return redirect()->route('products.index');
    }

    public function getReto(){
        $carts = Cart::where('user', Session::get('id'))->get();

        return view('reto', ['carts' => $carts->count()]);
    }
}
