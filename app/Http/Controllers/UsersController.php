<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Auth;

class UsersController extends Controller
{
    //
    public function getSignup(){
        return view('users.signup');
    }

    public function postSignup(Request $request){
        $this->validate($request, [
            'email' => 'email|required|unique:users',
            'password' => 'required|min:4'
        ]);

        $user = new Users([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
        ]);
        $user->save();

        //return redirect()->route('products.index');
        return redirect('/');
    }

    public function getSignin(){
        return view('users.signin');
    }

    public function postSignin(Request $request){
        $this->validate($request, [
            'email' => 'email|required',
            'password' => 'required|min:4'
        ]);

        if(Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])){
            $request->session()->put('id', Auth::id());
            $request->session()->put('email', $request->input('email'));
            $request->session()->put('name', Auth::user()->name);
            return redirect()->route('index');
        }

        return redirect()->back();
    }

    public function getLogout(){
        Auth::logout();
        return redirect('/');
    }
}
