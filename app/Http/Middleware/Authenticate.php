<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 14/03/19
 * Time: 03:58 PM
 */

namespace App\Http\Middleware;
use Closure;
use Auth;


class Authenticate
{
    public function handle($request, Closure $next, $guard =null){
        if(Auth::guard($guard)->guest()){
            if($request->ajax() || $request->wantsJson()){
                return response('Unauthorized', 401);
            }else{
                return redirect()->route('users.signin');
            }
        }
    }
}