<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $product = new \App\Product([
            'image_path' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg',
            'title' => 'Libro 1',
            'description' => 'Libro Harry Potter',
            'price' => '100'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg',
            'title' => 'Libro 2',
            'description' => 'Libro Harry Potter',
            'price' => '102'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg',
            'title' => 'Libro 3',
            'description' => 'Libro Harry Potter',
            'price' => '103'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg',
            'title' => 'Libro 4',
            'description' => 'Libro Harry Potter',
            'price' => '104'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg',
            'title' => 'Libro 5',
            'description' => 'Libro Harry Potter',
            'price' => '105'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'http://ecx.images-amazon.com/images/I/51ZU%2BCvkTyL.jpg',
            'title' => 'Libro 6',
            'description' => 'Libro Harry Potter',
            'price' => '106'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
            'title' => 'Celular 1',
            'description' => 'Celular Samsung',
            'price' => '100'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
            'title' => 'Celular 2',
            'description' => 'Celular Samsung',
            'price' => '102'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
            'title' => 'Celular 3',
            'description' => 'Celular Samsung',
            'price' => '103'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
            'title' => 'Celular 4',
            'description' => 'Celular Samsung',
            'price' => '104'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
            'title' => 'Celular 5',
            'description' => 'Celular Samsung',
            'price' => '105'
        ]);
        $product->save();

        $product = new \App\Product([
            'image_path' => 'https://static.toiimg.com/photo/64171591/Samsung-Galaxy-S10.jpg',
            'title' => 'Celular 6',
            'description' => 'Celular Samsung',
            'price' => '106'
        ]);
        $product->save();
    }
}
