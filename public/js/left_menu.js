let url = window.location.pathname;
let page = url.substring(url.lastIndexOf('/') + 1);
//console.log(page);

if(page.length === 0 || page === 'index'){
    $('#index').addClass('active');
}

if (page === 'products') {
	$('#shop').addClass('active');
	$('#'+page).addClass('active');
}
