<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 13/03/19
 * Time: 09:57 PM
 */
?>

<div class="menu">
    <ul class="list">
        <li class="header">MENÚ DE NAVEGACIÓN</li>
        <li id="index">
            <a href="/index">
                <i class="material-icons">home</i>
                <span>Inicio</span>
            </a>
        </li>
        <li id="shop">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">shopping_cart</i>
                <span>Comprar</span>
            </a>
            <ul class="ml-menu">
                <li id="products">
                    <a href="/products">Productos</a>
                </li>
            </ul>
        </li>
        <li id="index">
            <a href="/reto">
                <span>Reto Lógico</span>
            </a>
        </li>
    </ul>
</div>
