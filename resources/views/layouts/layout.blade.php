<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 13/03/19
 * Time: 08:54 PM
 */
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core Css -->
        {!! Html::style('css/bootstrap.min.css') !!}
        <!-- Waves Effect Css -->
        {!! Html::style('plugins/node-waves/waves.css') !!}
        <!-- Animation Css -->
        {!! Html::style('plugins/animate-css/animate.css') !!}
        <!-- Custom Css -->
        {!! Html::style('css/style.min.css') !!}
        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        {!! Html::style('css/themes/all-themes.css') !!}
        {!! Html::style('css/custom.css') !!}
        @yield('styles')
    </head>
    <body class="theme-red">
        @yield('content')
        <!-- Jquery Core Js -->
        {!! Html::script('js/jquery.min.js') !!}
        <!-- Bootstrap Core Js -->
        {!! Html::script('js/bootstrap.min.js') !!}
        <!-- Select Plugin Js -->
        {!! Html::script('plugins/bootstrap-select/js/bootstrap-select.js') !!}
        <!-- Slimscroll Plugin Js -->
        {!! Html::script('plugins/jquery-slimscroll/jquery.slimscroll.js') !!}
        <!-- Waves Effect Plugin Js -->
        {!! Html::script('plugins/node-waves/waves.js') !!}
        <!-- Custom Js -->
        {!! Html::script('js/admin.js') !!}
        <!-- Demo Js -->
        {!! Html::script('js/demo.js') !!}
        {!! Html::script('js/left_menu.js') !!}
        @yield('scripts')
    </body>
</html>
