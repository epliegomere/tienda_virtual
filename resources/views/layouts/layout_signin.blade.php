<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 13/03/19
 * Time: 08:54 PM
 */
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core Css -->
        {!! Html::style('css/bootstrap.min.css') !!}
        <!-- Waves Effect Css -->
        {!! Html::style('plugins/node-waves/waves.css') !!}
        <!-- Animation Css -->
        {!! Html::style('plugins/animate-css/animate.css') !!}
        <!-- Custom Css -->
        {!! Html::style('css/style.min.css') !!}
        @yield('styles')
    </head>
    <body class="login-page">
        @yield('content')
        <!-- Jquery Core Js -->
        {!! Html::script('js/jquery.min.js') !!}
        <!-- Bootstrap Core Js -->
        {!! Html::script('js/bootstrap.min.js') !!}
        <!-- Waves Effect Plugin Js -->
        {!! Html::script('plugins/node-waves/waves.js') !!}
        <!-- Validation Plugin Js -->
        {!! Html::script('plugins/jquery-validation/jquery.validate.js') !!}
        <!-- Custom Js -->
        {!! Html::script('js/admin.js') !!}
        {!! Html::script('js/pages/examples/sign-in.js') !!}
        @yield('scripts')
    </body>
</html>
