<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 13/03/19
 * Time: 09:10 PM
 */
?>

@extends('layouts.layout')

@section('title')
    Tienda Virtual - Productos
@endsection

@section('content')
    <!-- Top Bar -->
    <div id="not">
        <notifications total_qty="{{ Session::has('cart') ? Session::get('cart')->total_qty : '' }}"></notifications>
    </div>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Eduardo Pliego</div>
                    <div class="email">epliego@strappinc.net</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Perfil</a></li>
                            <li><a href="{{ route('users.signup') }}"><i class="material-icons">person</i>Registro Usuario</a></li>
                            <!--
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            -->
                            <li role="seperator" class="divider"></li>
                            <li><a href="/logout"><i class="material-icons">input</i>Desconectar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
        @include('include.left_menu')
        <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2019 <a href="javascript:void(0);">Tienda Virtual</a>.
                </div>
                <div class="version">
                    <b>Versión: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Productos</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Listado de Libros de la Tienda
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!--
                                    @foreach($products->chunk(3) as $productChunk)
                                        <div class="row">
                                            @foreach($productChunk as $product)
                                                <div class="col-sm-6 col-md-4">
                                                    <div class="thumbnail">
                                                        <img src="{{ $product->image_path }}" alt="..." class="img-responsive">
                                                        <div class="caption">
                                                            <h3>{{ $product->title }}</h3>
                                                            <p class="description">{{ $product->description }}</p>
                                                            <div class="clearfix">
                                                                <div class="pull-left price">Bs. {{ $product->price }}</div>
                                                                <a href="{{ route('products.addCart', ['id' => $product->id]) }}" class="btn btn-success pull-right" role="button">Añadir al carrito</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                    -->
                                        <div id="app">
                                            <products></products>
                                        </div>
                                        <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}" />
                                        <!-- Scripts -->
                                        {!! Html::script('js/app.js') !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
