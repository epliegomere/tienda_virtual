<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 14/03/19
 * Time: 08:36 PM
 */
?>

@extends('layouts.layout')

@section('title')
    Tienda Virtual - Carrito
@endsection

@section('content')
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/">Tienda</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">shopping_cart</i>
                            <!-- <span class="label-count">{{ Session::has('cart') ? Session::get('cart')->total_qty : '' }}</span> -->
                            <span class="label-count">{{ $carts }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">Productos en el carrito</li>
                            <li class="body">
                                <ul class="menu">

                                </ul>
                            </li>
                            <li class="footer">
                                @if($carts > 0)
                                    <a href="{{ route('products.shoppingCart') }}">Ver los productos</a>
                                @else
                                    No ha añadido productos al carrito
                                @endif
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="../../images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Eduardo Pliego</div>
                    <div class="email">epliego@strappinc.net</div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Perfil</a></li>
                            <li><a href="{{ route('users.signup') }}"><i class="material-icons">person</i>Registro Usuario</a></li>
                            <!--
                            <li role="seperator" class="divider"></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                            <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                            -->
                            <li role="seperator" class="divider"></li>
                            <li><a href="/logout"><i class="material-icons">input</i>Desconectar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
        @include('include.left_menu')
        <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2019 <a href="javascript:void(0);">Tienda Virtual</a>.
                </div>
                <div class="version">
                    <b>Versión: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>Carrito</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Listado de Productos en el carrito
                            </h2>
                        </div>
                        <div class="body">
                            @if($carts > 0)
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        @foreach($products as $product)
                                            <li class="list-group-item">
                                                <span class="badge">{{ $product['qty'] }}</span>
                                                <strong>{{ $product['item']['title'] }}</strong>
                                                <span class="label label-success">{{ $product['price'] }}</span>
                                            </li>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <strong>Total Bs. {{ $total_price }}</strong>
                                    </div>
                                </div>
                            @else
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h2>No ha añadido productos al carrito</h2>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
