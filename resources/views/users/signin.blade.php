<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 14/03/19
 * Time: 01:16 AM
 */
?>

@extends('layouts.layout_signin')

@section('title')
    Tienda Virtual - Inicio de Sesión
@endsection

@section('content')
    <div class="login-box">
        <div class="logo">
            <a href="/">Tienda Virtual</a>
        </div>
        <div class="card">
            <div class="body">
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <form id="sign_in" action="{{ route('users.signin') }}" method="POST">
                    <div class="msg">Inicio de sesión</div>
                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="email" placeholder="Correo electrónico" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Contraseña" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">INICIAR</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="/signup">Resgistrarse ahora!</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
