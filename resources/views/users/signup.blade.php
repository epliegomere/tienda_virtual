<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 14/03/19
 * Time: 01:15 AM
 */
?>

@extends('layouts.layout_signup')

@section('title')
    Tienda Virtual - Registro
@endsection

@section('content')
    <div class="signup-box">
        <div class="logo">
            <a href="/signup">Tienda Virtual</a>
            <small>Registro</small>
        </div>
        <div class="card">
            <div class="body">
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                @endif
                <form id="sign_up" action="{{ route('users.signup') }}" method="POST">
                    <div class="msg">Registro de nuevo usuario</div>
                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="name" placeholder="Nombre Completo" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">email</i>
                            </span>
                        <div class="form-line">
                            <input type="email" class="form-control" name="email" placeholder="Correo electrónico" required>
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" minlength="6" placeholder="Contraseña" required>
                        </div>
                    </div>
                    <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirma Contraseña" required>
                        </div>
                    </div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">REGISTRAR</button>
                </form>
            </div>
        </div>
    </div>
@endsection
