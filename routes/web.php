<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/add_to_cart/{id}', [
    'uses' => 'ProductController@getAddCart',
    'as' => 'products.addCart'
]);

Route::get('/shopping_cart', [
    'uses' => 'ProductController@getShoppingCart',
    'as' => 'products.shoppingCart'
]);

Route::group(['middleware' => 'guest'], function(){
    Route::get('/', function () {
        //return view('welcome');
        return view('users.signin');
    });

    Route::post('/signin', [
        'uses' => 'UsersController@postSignin',
        'as' => 'users.signin'
    ]);

    Route::get('/signup', [
        'uses' => 'UsersController@getSignup',
        'as' => 'users.signup'
    ]);
    Route::post('/signup', [
        'uses' => 'UsersController@postSignup',
        'as' => 'users.signup'
    ]);
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/index', [
        'uses' => 'ProductController@getIndex',
        'as' => 'index'
    ]);

    Route::get('/products', [
        'uses' => 'ProductController@getProducts',
        'as' => 'products.index'
    ]);

    Route::get('/logout', [
        'uses' => 'UsersController@getLogout',
        'as' => 'users.logout'
    ]);
    Route::get('/reto', [
        'uses' => 'ProductController@getReto',
        'as' => 'reto'
    ]);
});

Route::get('products_vue', 'ProductController@getVueProducts');

Route::get('products_vue/{id}', 'ProductController@getVueProduct');

Route::get('add_vue_cart/{id}', 'ProductController@getVueAddCart');
